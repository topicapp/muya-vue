# Muya-Vue

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

**Muya-Vue** is a Vue component to work with [muya](https://github.com/marktext/muya).

## Warning

:warning: Muya-Vue, in the same way as Muya, is still under development and should not be used for production. We hope to add a toolbar component in the future.

## Usage

```vue
<template>
  <div id="app">
    <MuyaEditor />
  </div>
</template>

<script>
import MuyaEditor from './components/Editor.vue';

export default {
  name: 'App',
  components: {
    MuyaEditor,
  },
};
</script>

<style lang="scss">
// overwrite theme colors this way
:root {
  --titleBarHeight: 32px;
  --editorAreaWidth: 750px;
  --editorAreaWidth: 750px;

  --themeColor: rgba(33, 181, 111, 1);
  --themeColor90: rgba(33, 181, 111, 0.9);
  --themeColor80: rgba(33, 181, 111, 0.8);
  --themeColor70: rgba(33, 181, 111, 0.7);
  --themeColor60: rgba(33, 181, 111, 0.6);
  --themeColor50: rgba(33, 181, 111, 0.5);
  --themeColor40: rgba(33, 181, 111, 0.4);
  --themeColor30: rgba(33, 181, 111, 0.3);
  --themeColor20: rgba(33, 181, 111, 0.2);
  --themeColor10: rgba(33, 181, 111, 0.1);

  --highlightColor: rgba(33, 181, 111, 0.4);
  --selectionColor: rgba(0, 0, 0, 0.1);
  --editorColor: rgba(0, 0, 0, 0.7);
  --editorColor80: rgba(0, 0, 0, 0.8);
  --editorColor60: rgba(0, 0, 0, 0.6);
  --editorColor50: rgba(0, 0, 0, 0.5);
  --editorColor40: rgba(0, 0, 0, 0.4);
  --editorColor30: rgba(0, 0, 0, 0.3);
  --editorColor10: rgba(0, 0, 0, 0.1);
  --editorColor04: rgba(0, 0, 0, 0.03);

  --editorBgColor: rgba(255, 255, 255, 1);
  --deleteColor: #ff6969;
  --iconColor: #6b737b;
  --codeBgColor: #d8d8d869;
  --codeBlockBgColor: rgba(0, 0, 0, 0.03);
  --inputBgColor: rgba(0, 0, 0, 0.06);
  --buttonBgColor: #ffffff;
  --buttonBorderColor: rgba(0, 0, 0, 0.2);
  --buttonShadow: rgba(0, 0, 0, 0.12);
  --buttonHover: #f2f2f2;
  --buttonActive: #e5e5e5;

  --sideBarColor: rgba(0, 0, 0, 0.6);
  --sideBarTitleColor: rgba(0, 0, 0, 1);
  --sideBarTextColor: rgba(0, 0, 0, 0.4);
  --sideBarBgColor: rgba(242, 242, 242, 0.9);
  --sideBarItemHoverBgColor: rgba(0, 0, 0, 0.03);
  --itemBgColor: rgba(255, 255, 255, 0.6);
  --floatBgColor: #fff;
  --floatHoverColor: rgba(0, 0, 0, 0.04);
  --floatBorderColor: rgba(0, 0, 0, 0.1);
  --floatShadow: rgba(15, 15, 15, 0.03) 0px 0px 0px 1px, rgba(15, 15, 15, 0.04) 0px 3px 6px,
    rgba(15, 15, 15, 0.05) 0px 9px 24px;
  --maskColor: rgba(255, 255, 255, 0.7);
  --tableBorderColor: rgba(0, 0, 0, 1);
}

// overwrite default styles this way
#app #muya-container {
  font-family: 'Roboto', Helvetica, Arial, sans-serif;
}
</style>
```

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Run your unit tests

```
npm run test:unit
```

### Lints and fixes files

```
npm run lint
```

## License

[MIT](https://gitlab.com/topicapp/muya-vue/-/blob/master/LICENSE) © [Topic App](https://gitlab.com/topicapp)
