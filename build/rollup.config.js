import typescript from '@rollup/plugin-typescript';
import replace from '@rollup/plugin-replace';
import url from '@rollup/plugin-url';
import json from '@rollup/plugin-json';
import commonjs from '@rollup/plugin-commonjs';
import vue from 'rollup-plugin-vue';
import postcss from 'rollup-plugin-postcss';
import { terser } from 'rollup-plugin-terser';

export default {
  input: 'src/plugin.ts',
  inlineDynamicImports: true,
  output: {
    name: 'MuyaVue',
    exports: 'named',
  },
  plugins: [
    replace({
      'process.env.NODE_ENV': JSON.stringify('production'),
    }),
    postcss(),
    url(),
    json(),
    commonjs(),
    typescript({ include: [/.tsx?$/, /.vue?.*?lang=ts/] }),
    vue({
      css: true, // Dynamically inject css as a <style> tag
      compileTemplate: true, // Explicitly convert template to render function
    }),
    terser(),
  ],
};
