// import virtualize from 'snabbdom-virtualize/strings'
import { init } from 'snabbdom/build/package/init';
import { classModule } from 'snabbdom/build/package/modules/class';
import { attributesModule } from 'snabbdom/build/package/modules/attributes';
import { styleModule } from 'snabbdom/build/package/modules/style';
import { propsModule } from 'snabbdom/build/package/modules/props';
import { datasetModule } from 'snabbdom/build/package/modules/dataset';
import { eventListenersModule } from 'snabbdom/build/package/modules/eventlisteners';
import { toVNode } from 'snabbdom/build/package/tovnode';

export const patch = init([
  // Init patch function with chosen modules
  classModule, // makes it easy to toggle classes
  attributesModule,
  styleModule, // handles styling on elements with support for animations
  propsModule, // for setting properties on DOM elements
  datasetModule,
  eventListenersModule, // attaches event listeners
]);
export { h } from 'snabbdom/build/package/h'; // helper function for creating vnodes
export { default as toHTML } from 'snabbdom-to-html'; // helper function for convert vnode to HTML string
// helper function for convert DOM to vnode
export { toVNode };

export const htmlToVNode = html => {
  // helper function for convert html to vnode
  const wrapper = document.createElement('div');
  wrapper.innerHTML = html;
  return toVNode(wrapper).children;
};
