import { PluginObject } from 'vue';
// Import vue component
import Editor from './components/Editor.vue';

// Create module definition for Vue.use()
const plugin: PluginObject<{}> = {
  install(Vue, options) {
    if (plugin.installed) return;
    plugin.installed = true;
    Vue.component('MuyaEditor', Editor);
  },
};

// Auto-install when vue is found (eg. in browser via <script> tag)
let GlobalVue = null;
if (typeof window !== 'undefined') {
  GlobalVue = window.Vue;
} else if (typeof global !== 'undefined') {
  GlobalVue = global.Vue;
}
if (GlobalVue) {
  GlobalVue.use(plugin);
}

// To allow use as module (npm/webpack/etc.) export component
export default Editor;
